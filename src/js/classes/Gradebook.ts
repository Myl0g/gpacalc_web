import Grade from "./Grade";

export default class Gradebook {
  public grades: Grade[];

  constructor(grades?: [Grade]) {
    this.grades = grades || [];
  }

  get gpa(): number {
    let undividedGPA = 0.0;

    for (const grade of this.grades) {
      let singleGradeWorth = grade.letter;
      singleGradeWorth += grade.symbol; // Apply symbol
      singleGradeWorth *= grade.type; // Apply weight
      undividedGPA += singleGradeWorth;
    }

    return undividedGPA / this.grades.length;
  }

  get unweighted(): number {
    let undivided = 0.0;

    for (const grade of this.grades) {
      let gradeWorth = grade.letter;
      if (grade.letterReadable + grade.symbolReadable !== "A+") {
        gradeWorth += grade.symbol; // Apply symbol
      }
      undivided += gradeWorth;
    }

    return undivided / this.grades.length;
  }

  public save(): void {
    // stackoverflow.com/questions/532635/javascript-cookie-with-no-expiration-date
    document.cookie = `gradebook=${JSON.stringify(this)}; expires=Thu, 18 Dec " + ${new Date().getFullYear() +
      1} + " 12:00:00 UTC"`;
  }
}
