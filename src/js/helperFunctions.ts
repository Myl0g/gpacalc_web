import Grade from "./classes/Grade";
import Gradebook from "./classes/Gradebook";

/**
 * Creates a new HTMLSelectElement with the specified options.
 * @param options String array of options for the element. Value is made equivalent to the displayed text.
 */
export function createSelectElement(options: string[]): HTMLSelectElement {
  const result: HTMLSelectElement = document.createElement("select");
  let optionStr = "";
  for (const option of options) {
    optionStr += `<option val="${option}">${option}</option>`;
  }
  result.innerHTML = optionStr;
  return result;
}

/**
 * Inserts an input box and 3 select elements to input grade information into the provided document.
 * @param document The HTMLDocument to insert under. Ensure a div with the id `"main-div"` exists.
 * @param courseName Optional: the course name to use as the default value with the input box.
 * @param letter Optional: the letter grade to use as the default value for the relevant select element.
 * @param symbol Optional: the symbol (+/-) to use as the default value for the relevant select element.
 * @param type Optional: the type (Reg/Honors/Accel) to use as the default value for the relevant select element.
 */
export function createNewGradeSlot(
  document: HTMLDocument,
  courseName?: string,
  letter?: string,
  symbol?: string,
  type?: string,
): void {
  const newSlot: HTMLInputElement = document.createElement("input");

  newSlot.placeholder = "Course Name";
  if (courseName) {
    newSlot.value = courseName;
  }

  (document.getElementById("main-div") as HTMLDivElement).appendChild(newSlot);

  const letterElem = createSelectElement(["A", "B", "C", "D", "F"]);
  letterElem.value = letter || "A";

  (document.getElementById("main-div") as HTMLDivElement).appendChild(
    letterElem,
  );

  const symbolElem = createSelectElement(["+", "-", "None"]);
  symbolElem.value = symbol || "+";
  (document.getElementById("main-div") as HTMLDivElement).appendChild(
    symbolElem,
  );

  const typeElem = createSelectElement(["Regular", "Honors", "Accelerated"]);
  typeElem.value = type || "Regular";
  (document.getElementById("main-div") as HTMLDivElement).appendChild(typeElem);

  (document.getElementById("main-div") as HTMLDivElement).appendChild(
    document.createElement("br"),
  );
}

/**
 * Translates a HTMLCollection into an array of Grades.
 * This collection should contain at least one HTMLInputElement, followed by two Select elements.
 * The input element should contain the name of the course.
 * The first select element should contain the +/- symbol, and the second should contain the type of course.
 * @param grades The HTMLCollection to use.
 */
export function htmlToGrades(grades: HTMLCollection): Grade[] {
  const arr = [].slice.call(grades);
  const result: Grade[] = [];

  let name: string;
  let letter: string;
  let symbol: string;
  let type: string;
  for (const elem of arr) {
    if (elem instanceof HTMLInputElement) {
      name = (elem as HTMLInputElement).value;
    } else if (elem instanceof HTMLSelectElement) {
      const select = elem as HTMLSelectElement;
      if (select.value === "+" || select.value === "-") {
        symbol = select.value;
      } else if (
        select.value === "Regular" ||
        select.value === "Honors" ||
        select.value === "Accelerated"
      ) {
        type = select.value;
      } else {
        letter = select.value;
      }
    }

    if (name && letter && symbol && type) {
      result.push(new Grade(name, letter, symbol, type));
      name = undefined;
      letter = undefined;
      symbol = undefined;
      type = undefined;
    }
  }

  return result;
}

export function loadGradebook(): Gradebook {
  const json = document.cookie.split("gradebook=")[1].split(";")[0];
  return JSON.parse(json);
}
